# Thibaut Poullain 2020

class PurgePropositionsGdpr < ActiveJob::Base
  # HFAR-585 règle 3
  # --
  # L'objectif de ce job est de purger les contrats client matchant les règles RGPD préétablies
  # Le français était la norme dans la codebase
  #
  # Principe :
  # prendre prendre les Contrat clients
  # Quand leurs status sont à : CPV rectractés OU KO distrib, OU CPV Annulés OU Cpv Supprimés
  # ET Lorsque la date du status est supérieur à 7 ans.
  # ALORS
  # On supprime les data relatives suivantes : Fiche proposition, tâches, souscription, signature, outgoing_emails
  # document et document façonniers.
  # Si le client n'a plus de contrats, alors on le supprime ainsi que le mandat.
  # ---
  # On supprime toutes les souscription dont la date de rejet est supérieure à 7 ans.

  def perform(personne_ids = nil)

    @limit                = SystemSetting['duree_conservation_contrats_annulles'].years.ago
    @personne_ids         = personne_ids
    @propositions_cibles  = get_propositions
    @souscriptions_cibles = get_souscriptions
    @clients_cibles       = propositions_cibles.map(&:client)

    # AppFlow est un gestionnaire de job custom réalisé en interne
    AppFlow.run('PurgePropositionsGdpr#perform') do |flow|
      @flow           = flow
      @flow.ko_number = 0
      @flow.ok_number = 0

      purge_propositions
      purge_souscriptions
      purge_clients
    end
  end

  private

  def get_propositions

    proposition = Proposition.arel_table
    pers_ar     = Personne.arel_table

    result = Proposition.left_outer_joins(:contacts, :client, :factures)

    result = result.where('propositions.signataire_id in (?)', @personne_ids) if @personne_ids

    result = result.contrat_happe
                   .where(proposition[:retractation].lt(@limit)
                          .or(proposition[:rejet_distrib].lt(@limit))
                          .or(proposition[:annulee].lt(@limit))
                          .or(proposition[:date_validite].lt(Date.today)
                              .and(proposition[:validee].eq(nil))
                              .and(proposition[:activee].eq(nil))
                              .and(proposition[:demande_distrib].eq(nil))
                              .and(proposition[:date_resiliation].eq(nil))
                              .and(proposition[:resiliee].eq(nil))
                              .and(proposition[:date_validite].lt(@limit))
                              .and(proposition[:rejet_demande].eq(nil))
                              .and(proposition[:annulee].eq(nil))
                              .and(proposition[:retractation].eq(nil))
                              .and(proposition[:rejet_distrib].eq(nil))))
                   .where('personnes.date_debut_reclamation IS null OR personnes.date_debut_reclamation <= ?', Date.today)
                   .where(pers_ar[:id].not_in(GdprService::CLIENT_EXCLUDED))
                   .having('sum(coalesce(factures.montant_restant_du, 0)) = 0')
                   .group('propositions.id')
    result
  end

  def get_souscriptions
    prop_ar = Proposition.arel_table

    result = HappeSouscription.joins(:proposition)
                              .where('rejetee_a < ?', @limit)
                              .where(prop_ar[:signataire_id].not_in(GdprService::CLIENT_EXCLUDED))

    result = result.where('propositions.signataire_id in (?)', @personne_ids).distinct if @personne_ids

    result
  end

  def in_rescueable_transaction
    ActiveRecord::Base.transaction do
      begin
        yield if block_given?
      rescue ActiveRecord::RecordNotDestroyed => error
        @flow.info "errors : #{error.inspect}"
        @flow.info "reccord : #{error.record.inspect}"
        @flow.info "messages : #{error.record.errors.messages.inspect}"
        @flow.ko_number += 1
      end
    end
  end

  def purge_clients
    in_rescueable_transaction do
      @clients_cibles.each do |client|
        next if Proposition.where(signataire_id: client.id).any?

        client.try(:compte_client).try(:destroy!)
        result = client.try(:destroy!)

        update_flow(client, result)
      end
    end
  end

  def purge_propositions
    in_rescueable_transaction do
      @propositions_cibles.each do |proposition|

        proposition.contacts.each(&:destroy!)
        proposition.taches.each(&:destroy!)
        proposition.souscription.destroy! if proposition.souscription.present?
        proposition.documents.each(&:destroy!)
        result = proposition.destroy!

        update_flow(proposition, result)

        local_livraison = proposition.local_livraison
        next if Proposition.where(local_livraison_id: local_livraison.id).contrat_happe.any?
        local_livraison.propositions.each(&:destroy!)
        local_livraison.destroy!
      end
    end
  end

  def purge_souscriptions
    in_rescueable_transaction do
      @souscriptions_cibles.each(&:destroy!)
    end
  end

  def update_flow(object, result)
    if result
      @flow.info "#{object.class.name} #{object.id} SUPPRIMEE"
      @flow.ok_number += 1
    else
      @flow.ko_number += 1
    end
  end
end
