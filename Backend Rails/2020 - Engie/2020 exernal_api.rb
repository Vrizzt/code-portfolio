module BuyapowaServices
  class Api

    # TODO - put that in mixins
    HTTP_OK_CODE                         = 200
    HTTP_OK_CREATION_CODE                = 201
    HTTP_BEING_PROCEED_CODE              = 202
    HTTP_ACCEPTED_FOR_REMOTE_ACCESS_CODE = 303

    API_ENDPOINT  = 'https://api.co-buying.com/api/referrals'.freeze

    # WIP available but unused constants
    MARKET_NAME   = 'Happe-e'.freeze
    MARKET_DOMAIN = 'happ-e.co-buying.com'.freeze
    CAMPAIN_SLUG  = 'hapee_raf1'.freeze
    SECRET_KEY    = ENV['buyapowa_secret_key']

    # params : hash : {order_number: STRING, status: [confirmed failed manual-pass restore]}
    def self.send_reconciliation(params, proposition_id)
      request(http_method: :post, endpoint: 'confirmations', params: params, proposition_id: proposition_id)
    end

    private

    def self.request(http_method:, endpoint:, params: {}, proposition_id:)
      response = client.public_send(http_method, endpoint, parse_to_payload(params, proposition_id))
      return true if response_successful?(response)

      create_web_service_anomaly(
        message: JSON.parse(response.body),
        proposition_id: proposition_id,
        params: params,
        personne_id: Proposition.find(proposition_id).client.id,
        code_retour: response.status,
        request_path: "#{API_ENDPOINT}/#{endpoint}")
    end

    def self.client
      Faraday.new(API_ENDPOINT) do |connexion|
        connexion.request :url_encoded
        connexion.adapter Faraday.default_adapter
        connexion.headers = {
          'Content-Type' => 'application/json',
          'auth_token'   => ENV['buyapowa_auth_token'],
          'auth_id'      => ENV['buyapowa_market_id'],
          'user_agen'    => 'Engie Agilab API'
        }
      end
    end

    def self.parse_to_payload(params, proposition_id)
      return {order: {
                order_number: params[:order_number],
                status: params[:status]
              }
      }.to_json if validate_params(params).empty?

      create_web_service_anomaly(
        message: "Invalid parameters : #{validate_params(params).join(', ')}",
        proposition_id: proposition_id,
        personne_id: Proposition.find(proposition_id).client.id,
        params: params)
    end

    def self.create_web_service_anomaly(message:, proposition_id:, params:, code_retour: nil, request_path: nil, personne_id:)
      message = "#{self.name}##{caller_locations.first.label} - #{message}"
      ws = WebServiceAnomaly.new(
        proposition_id: proposition_id,
        message: message,
        code_retour: code_retour,
        personne_id: personne_id,
        donnees_en_entree: params,
        type_de_ws: WebServiceAnomaly::TYPE_WS__API_BUYAPOWA,
        request_path: request_path)
        ws.save!
    end

    def self.validate_params(params)
      error = []
      status_error       = "Status #{params[:status]} donné. Doit être l'un d'entre eux : confirmed failed manual-pass restore"
      order_number_error = 'order_number non renseigné. Obligatoire.'

      status_test        = %w[confirmed failed manual-pass restore].include?(params[:status])
      order_number_test  = params[:order_number].present?

      error << status_error       unless status_test
      error << order_number_error unless order_number_test
      error
    end

    def self.response_successful?(response)
      response.status == (HTTP_OK_CODE || HTTP_OK_CREATION_CODE || HTTP_BEING_PROCEED_CODE)
    end
  end
end
