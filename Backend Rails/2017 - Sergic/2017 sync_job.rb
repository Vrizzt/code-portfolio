# Thibaut Poullain 2017
# Extrait de Job de synchronisation avec Querry builder

module QueryObject
  class AllocatedBudgetQuery
    attr_reader :relation, :filters

    def self.call(filters = {}, relation = ::AllocatedBudget.all)
      new(filters, relation).tap(&:call).relation
    end

    def initialize(filters = {}, relation = ::AllocatedBudget.all)
      @relation     = relation
      @filters      = filters
    end

    private
    def call
      conditions = {
        budget_previsionnel: { supprime: 'false' },
        budget: { supprime: 'false' }
      }

      # place filter
      conditions = conditions.merge(immeuble: { id: @filters[:place_id] }) if @filters.key?(:place_id)
      # Note 2022
      @filters.key?(:place_id) and conditions = conditions.merge(immeuble: { id: @filters[:place_id] })


      # situation budget filter
      conditions = conditions.merge(situation_budget: { codesituation: @filters[:budget_state] }) if @filters.key?(:budget_state)
      # Note 2022
      @filters.key?(:budget_state) and conditions = conditions.merge(situation_budget: { codesituation: @filters[:budget_state] })

      # main extract
      @relation = @relation.joins(:account_code,
                                  account_code: [:place],
                                  budget: %i[fiscal_year budget_state])
                           .where(conditions)

      # current fiscal year filter
      @relation = @relation.where("'#{Date.today.to_s}' between exercice_comptable.datedebutexercice and exercice_comptable.datefinexercice") if @filters.key?(:current_fiscal_year)
    end
  end
end


class PlaceInitJob < ApplicationJob
  def perform(place_id = nil)
    Appsignal.monitor_transaction(
      'perform_job.sync_eseis.account_code.job',
      class: 'SYNC',
      method: self.class.name,
      queue_start: Time.zone.now
    ) do
        place = place_id.nil? ?
          ::Place.where(pack_id: ENV['ESEIS_PACK_ID']) :
          [::Place.find(place_id)]

        place.each do |l|
          l.fiscal_years.each do |crm_fiscal_year|
            Init::SendFiscalYearJob.perform_later(crm_fiscal_year)
          end

          l.account_codes.each do |crm_account_code|
            Init::SendAccountCodeJob.perform_later(crm_account_code)
          end

          filters = { place_id: l.id, budget_state: %w[A V] }
          QueryObject::AllocatedBudgetQuery.call(filters).each do |crm_allocated_budget|
            Init::SendAllocatedBudgetJob.perform_later(crm_allocated_budget)
          end

          l.account_place_entries
           .where(supprime: false)
           .each do |crm_account_place_entry|
            Init::SendAccountPlaceEntryJob.perform_later(l.id, crm_account_place_entry)
          end
        end
      end
    end
  end
end