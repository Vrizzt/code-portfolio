# frozen_string_literal: true

module ActionsForRecord
  # polymorphic actions handler for records
  # actions is a hash :
  #
  # { user: { lambda, lambda_params },
  #   company: { lambda, lambda_params },
  #   (...) ,
  #   unknown: { lambda, lambda_params }
  # }
  def self.perform(record, actions)
    record_name   = record.is_a?(Symbol) ? record : record.class.name.downcase.to_sym
    action        = actions[record_name]&.first
    action_params = actions[record_name]&.last

    action.present? and action_params.present? and return action.call(action_params)
    action.present? and return action.call
  end
end
