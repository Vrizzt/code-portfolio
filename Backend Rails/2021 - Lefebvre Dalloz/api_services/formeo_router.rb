# frozen_string_literal: true

module ApiServices
  class FormeoRouter
    extend ApigeeAuthenticable

    FUF_API_PREFIX    = '/formeo-gateway/fuf-api'
    APIGEE_API_PREFIX = '/formeo/v1'

    def self.get_token
      Rails.env.development? || Rails.env.test? and
        return Rails.application.credentials[Rails.env.to_sym][:FORMEO_TOKEN].freeze

      apigee_access_token
    end

    def self.get_endpoint
      apigee = apigee_endpoint
      formeo = Rails.application.credentials[Rails.env.to_sym][:FORMEO_URL].freeze
      self.router(formeo, apigee)
    end

    def self.endpoint_get_companies_by
      params = '?jsonNamedConfig=advancedsearch'
      formeo = "#{FUF_API_PREFIX}/etablissement/search#{params}"
      apigee = "#{APIGEE_API_PREFIX}/establishments/search#{params}"
      self.router(formeo, apigee)
    end

    def self.endpoint_get_updated_companies(last_updated)
      params = "?full=true&jsonNamedConfig=quicksearch&lastUpdated=#{last_updated}"
      formeo = "#{FUF_API_PREFIX}/etablissements#{params}"
      apigee = "#{APIGEE_API_PREFIX}/establishments#{params}"
      self.router(formeo, apigee)
    end

    def self.endpoint_get_users_by
      formeo = "#{FUF_API_PREFIX}/interlocuteur/search"
      apigee = "#{APIGEE_API_PREFIX}/interlocutors/search"
      self.router(formeo, apigee)
    end

    def self.endpoint_get_updated_users(last_updated)
      params = "?full=true&jsonNamedConfig=only_id&lastUpdated=#{last_updated}"
      formeo = "#{FUF_API_PREFIX}/interlocuteurResumes#{params}"
      apigee = "#{APIGEE_API_PREFIX}/interlocutor-summaries#{params}"
      self.router(formeo, apigee)
    end

    def self.endpoint_get_users_by_ids
      formeo = "#{FUF_API_PREFIX}/interlocuteurResume/findInterlocuteurResumesById"
      apigee = "#{APIGEE_API_PREFIX}/interlocutor-summaries/search"
      self.router(formeo, apigee)
    end

    def self.endpoint_get_sessions_inter_between(url_params = {})
      formeo = "/formeo-gateway/ldc/inscription-api/session/sessionExams#{get_users_between_params(url_params)}"
      apigee = "#{APIGEE_API_PREFIX}/registration-exam-sessions#{get_users_between_params(url_params)}"
      self.router(formeo, apigee)
    end

    def self.endpoint_get_sessions_intra_between(url_params = {})
      formeo = "/formeo-gateway/ldc/surmesure-api/sessionSurMesure/sessionExams#{get_users_between_params(url_params)}"
      apigee = "#{APIGEE_API_PREFIX}/custom-exam-sessions#{get_users_between_params(url_params)}"
      self.router(formeo, apigee)
    end

    def self.endpoint_import_inscription_inter(url_params = {})
      formeo = '/formeo-gateway/ldc/inscription-api/inscription/importInscription'
      apigee = "#{APIGEE_API_PREFIX}/registration-import"
      self.router(formeo, apigee)
    end

    private
      def self.get_users_between_params(url_params)
        return '' unless url_params.any?

        "?startDate=#{url_params[:formeo_start_date]}"\
        "&endDate=#{url_params[:formeo_end_date]}"\
        "#{url_params[:formeo_certification]}"\
        "#{url_params[:formeo_json_named_config]}"
      end

      def self.router(formeo, apigee = nil)
        Rails.env.development? || Rails.env.test? ? formeo : apigee
      end
  end
end
