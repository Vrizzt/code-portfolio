# frozen_string_literal: true

module FormeoServices
  # ONLY Responsible for creating instance of Ours record based on formeo record.
  # NOT responsible for saving them.
  # ALWAYS return an Ours record. If errors occurs, the ours record is invalid and has errors
  # in the usual place : record.errors.messages
  class ModelConverter
    include ActionsForRecord

    USER_SYNCED_ATTRIBUTES = {
      email:                      nil,
      first_name:                 nil,
      last_name:                  nil,
      genre:                      nil,
      admin:                      nil,
      super_admin:                nil,
      id_fuf:                     nil,
      postal_address:             nil,
      postal_address_complement:  nil,
      postal_address_city:        nil,
      postal_address_code:        nil,
      postal_address_country:     nil,
      company_id:                 nil
    }

    COMPANY_SYNCED_ATTRIBUTES = {
      name:                       nil,
      certif_validation:          nil,
      raison_sociale:             nil,
      adresse:                    nil,
      code_postal:                nil,
      ville:                      nil,
      siret:                      nil,
      id_fuf:                     nil
    }

    def self.convert(ours_record_name, record_formeo, **ours_custom_attributes)
      ours_user_params    = -> (lambda_record_formeo) { filter_user_params(lambda_record_formeo) }
      ours_company_params = -> (lambda_record_formeo) { filter_company_params(lambda_record_formeo) }

      actions             = { user:    [ours_user_params, record_formeo],
                              company: [ours_company_params, record_formeo] }

      ours_record_params  = ActionsForRecord.perform(ours_record_name, actions)
      record              = ours_record_name.to_s.capitalize.constantize.new(ours_record_params)

      complete_and_validate_with(record, **ours_custom_attributes)
    end

    def self.convert_date_to_formeo_date(date)
      date = Date.strptime(date, OURS_INPUT_DATE_FORMAT) unless date.is_a?(ActiveSupport::TimeWithZone) || date.is_a?(Date)
      date.strftime(FORMEO_DATE_FORMAT)
    end

    def self.convert_date_to_ours_date(date)
      date.strftime(OURS_INPUT_DATE_FORMAT)
    end

    private
      ## CONSTANTS
      FORMEO_DATE_FORMAT          = '%Y-%m-%d'
      OURS_INPUT_DATE_FORMAT      = '%d/%m/%Y'
      FORMEO_ISO_DATE_TIME_FORMAT = '%Y-%m-%dT%k:%M:%S'

      def self.complete_and_validate_with(record, **ours_custom_attributes)
        begin
          raise ActiveModel::MissingAttributeError.new('id_fuf missing') unless record.id_fuf
          ours_custom_attributes.each { |k, v| record.write_attribute(k.to_s, v) } if ours_custom_attributes.any?
          record.valid?
        rescue ActiveModel::MissingAttributeError, ArgumentError => e
          record.errors.add(:error, message: e.message)
        end
        record
      end

      def self.filter_company_params(company_formeo)
        company_params = COMPANY_SYNCED_ATTRIBUTES.deep_dup

        company_params[:name]              = company_formeo['raisonSociale']
        company_params[:certif_validation] = 999 # TODO - à spécifier dans front, notemment pour l'amf,
        company_params[:raison_sociale]    = company_formeo&.dig('raisonSociale')
        company_params[:adresse]           = company_formeo&.dig('etablissementAdressePostale', 'denominationRue')
        company_params[:code_postal]       = company_formeo&.dig('etablissementAdressePostale', 'codePostal')
        company_params[:ville]             = company_formeo&.dig('etablissementAdressePostale', 'ville')
        company_params[:siret]             = company_formeo&.dig('numSiret')
        company_params[:id_fuf]            = company_formeo&.dig('id')

        company_params
      end

      def self.filter_user_params(user_formeo)
        user_params = USER_SYNCED_ATTRIBUTES.deep_dup

        user_params[:email]                     = user_formeo&.dig('email')
        user_params[:first_name]                = user_formeo&.dig('prenom')
        user_params[:last_name]                 = user_formeo&.dig('nom')
        user_params[:genre]                     = user_formeo&.dig('civiliteLibelle') == 'Madame' ? 'f' : 'h'
        user_params[:admin]                     = false
        user_params[:super_admin]               = false
        user_params[:id_fuf]                    = user_formeo&.dig('id')
        user_params[:postal_address]            = user_formeo&.dig('voieNom')
        user_params[:postal_address_complement] = user_formeo&.dig('rnvpComplementAdresse')
        user_params[:postal_address_city]       = user_formeo&.dig('ville')
        user_params[:postal_address_code]       = user_formeo&.dig('codePostal')
        user_params[:postal_address_country]    = user_formeo&.dig('paysLibelle')

        user_params
      end
  end
end
