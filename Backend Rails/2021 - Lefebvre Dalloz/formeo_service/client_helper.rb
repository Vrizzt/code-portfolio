# frozen_string_literal: true

module FormeoServices
  class ClientHelper
    STUDENT_FORMEO_FONCTION_ID = '112'
    STUDENT_PACKS = %w[AMF-Pk-ESS-etu AMF-Pk-PREM-livre-etu AMF-Pk-PREM-ebook-etu ETUFD-Pk-Express]

    def self.build_import_inscription_inter_params(pack, user, grouping)
      {
        etablissement: FormeoServices::ClientHelper.extract_etablissement(pack),
        interlocuteur: FormeoServices::ClientHelper.extract_interlocuteur(pack, user),
        codeMarketing: pack.product.formeo_code_marketing,
        correlationId: SecureRandom.urlsafe_base64(20),
        ours: {
          dateDebut: FormeoServices::ModelConverter.convert_date_to_ours_date(Time.zone.today),
          regroupement: "_#{grouping.id}",
          pack: "_#{pack.product.id}_#{pack.duration}"
        }
      }
    end

    private
      def self.extract_etablissement(pack)
        uac = pack.user_and_company

        return unless uac.professional

        company = Company.find_by(siret: uac.siret.strip) unless uac.siret&.strip.empty?

        return { id: company.id_fuf } unless company.nil?

        {
          nom: uac.company,
          email: uac.email,
          adresse: uac.address,
          complement: uac.complement_address,
          ville: uac.city,
          codePostal: uac.postal_code,
          pays: uac.country,
          telephone: uac.phone_number,
          siret: uac.siret&.strip,
          numeroTva: uac.tva_number
        }
      end

      def self.extract_interlocuteur(pack, user)
        return { id: user.id_fuf } unless user.nil?

        uac = pack.user_and_company

        fonction_exacte = if STUDENT_PACKS.include?(pack.boutique_id)
          "#{uac.function};#{STUDENT_FORMEO_FONCTION_ID}"
        else
          uac.function
        end

        {
          civiliteId: uac.civility == 'h' ? 1 : 2,
          nom: uac.lastname,
          prenom: uac.firstname,
          email: uac.email,
          fonctionExacte: fonction_exacte,
          adresse: uac.address,
          complement: uac.complement_address,
          ville: uac.city,
          codePostal: uac.postal_code,
          pays: uac.country,
          telephone: uac.phone_number,
        }
      end
  end
end
