# frozen_string_literal: true

module FormeoServices
  # ONLY Responsible for handling the business logic of saving the records validated by the converter
  # ALWAYS return an Ours record. If errors occurs, the ours record is invalid and has errors
  # in the usual place : record.errors.messages
  class Synchronizer
    include ActionsForRecord

    def self.sync_user(id_fuf, **custom_record_attributes)
      return handle_error_for(User, :id_fuf, "Aucun identifiant Formeo n'a été renseigné") if id_fuf.blank?

      user_formeo = FormeoServices::Client.get_users_by_ids(ids: [id_fuf]).first

      return handle_error_for(User, :id_fuf, 'Error 404 - Not Found') if user_formeo.nil?

      # handle professional users, sync the company before syncing the user
      if user_formeo['etablissementId']
        company = sync_company(user_formeo['etablissementId'])
        return handle_error_for(User, :company_id, company.errors.full_messages.join(', ')) if company.errors.any?
        custom_record_attributes.merge!({ company_id: company.id })
      end

      user_ours = FormeoServices::ModelConverter.convert(:user, user_formeo, **custom_record_attributes)

      create_or_update user_ours
    end

    def self.sync_company(id_fuf, **custom_record_attributes)
      return handle_error_for(Company, :id_fuf, "Aucun identifiant Formeo n'a été renseigné") if id_fuf.blank?

      company_formeo = FormeoServices::Client.get_companies_by({ id: id_fuf }).first

      return handle_error_for(Company, :id_fuf, 'Error 404 - Not Found') if company_formeo.nil?

      company_ours = FormeoServices::ModelConverter.convert(:company, company_formeo, **custom_record_attributes)

      create_or_update company_ours
    end

    private
      def self.handle_error_for(record_class, attribute_name, error_message)
        record = record_class.new
        record.errors.add(attribute_name, :invalid, message: error_message)
        record
      end

      def self.create_or_update(record)
        existing_record = get_existing(record)

        begin
          if existing_record

            generic_updater = -> (lambda_record) { existing_record.update!(filtered_params(lambda_record)) }

            user_udpater    = -> (lambda_record) do
              existing_record.update!(filtered_params(lambda_record))
              InscriptionServices::CoachingReportCertificatedService.update_exported(existing_record)
            end

            actions = { user:    [ user_udpater, record ],
                        company: [ generic_updater, record ] }

            ActionsForRecord.perform(record, actions)
            return existing_record
          end

          record if record.save!
        rescue ActiveRecord::RecordInvalid
          record
        end
      end

      def self.get_existing(record)
        generic_finder = -> (lambda_record) { lambda_record.class.find_by(id_fuf: lambda_record.id_fuf) if lambda_record.id_fuf }

        user_finder    = -> (lambda_record) do
          existing_record = nil
          existing_record = lambda_record.class.find_by(id_fuf: lambda_record.id_fuf) if lambda_record.id_fuf
          existing_record ||= User.find_by(email: lambda_record.email) if lambda_record.email
          existing_record
        end

        actions = { user:   [ user_finder, record ],
                    company: [ generic_finder, record] }

        ActionsForRecord.perform(record, actions)
      end

      def self.filtered_params(record)
        user_params_filter    = -> { FormeoServices::ModelConverter::USER_SYNCED_ATTRIBUTES }
        company_params_filter = -> { FormeoServices::ModelConverter::COMPANY_SYNCED_ATTRIBUTES }

        actions = { user:    [ user_params_filter, nil ],
                    company: [ company_params_filter, nil ] }

        record_filtered_params = ActionsForRecord.perform(record, actions)

        record.attributes.select! { |key, value| value.present? && record_filtered_params.keys.map(&:to_s).include?(key) }
      end
  end
end
