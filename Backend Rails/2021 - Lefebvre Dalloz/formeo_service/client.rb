# frozen_string_literal: true

module FormeoServices
  class Client
    def self.get_companies_by(params, **configuration_params)
      handle_nested_elements request(http_method: :post,
                                     endpoint: ApiServices::FormeoRouter.endpoint_get_companies_by,
                                     params: parse_params(required_params: params,
                                                          allowed_params: COMPANY_ALLOWED_PARAMS,
                                                          **configuration_params))
    end

    def self.get_users_by(params, **configuration_params)
      handle_nested_elements request(http_method: :post,
                                     endpoint: ApiServices::FormeoRouter.endpoint_get_users_by,
                                     params: parse_params(required_params: params,
                                                          allowed_params: USER_ALLOWED_PARAMS,
                                                          **configuration_params))
    end

    ## returns id_fuf of updated companies from Formeo endpoint
    def self.get_updated_companies(last_updated_time)
      last_updated_param = last_updated_param(last_updated_time)
      handle_nested_elements request(http_method: :get,
                                     endpoint: ApiServices::FormeoRouter.endpoint_get_updated_companies(last_updated_param))

    end

    ## returns id_fuf of updated users from Formeo endpoint
    def self.get_updated_users(last_updated_time)
      last_updated_param = last_updated_param(last_updated_time)
      handle_nested_elements request(http_method: :get,
                                     endpoint: ApiServices::FormeoRouter.endpoint_get_updated_users(last_updated_param))
    end

    def self.get_users_by_ids(ids: [])
      handle_elements request(http_method: :post,
                              endpoint: ApiServices::FormeoRouter.endpoint_get_users_by_ids,
                              params: parse_params(required_params: { ids: ids },
                                                   allowed_params: USER_ALLOWED_PARAMS))
    end

    def self.get_sessions_inter_between(start_date, end_date, certification = nil, inscription_token_only = true)
      url_params = session_exam_params(start_date,
                                       end_date,
                                       certification,
                                       inscription_token_only)

      handle_elements request(http_method: :get,
                              endpoint: ApiServices::FormeoRouter.endpoint_get_sessions_inter_between(url_params))
    end

    def self.get_sessions_intra_between(start_date, end_date, certification = nil, inscription_token_only = true)
      url_params = session_exam_params(start_date,
                                       end_date,
                                       certification,
                                       inscription_token_only)

      handle_elements request(http_method: :get,
                              endpoint: ApiServices::FormeoRouter.endpoint_get_sessions_intra_between(url_params))
    end

    def self.get_formeo_users_between(date_start, date_end, certification = nil, inscription_token_only = true)
      extractor = -> (response) { response.is_a?(Array) ? response : [] }

      users_inter_resp = get_sessions_inter_between(date_start, date_end, certification, inscription_token_only)
      users_intra_resp = get_sessions_intra_between(date_start, date_end, certification, inscription_token_only)

      [extractor.call(users_inter_resp), extractor.call(users_intra_resp)]
    end

    def self.get_formeo_url
      Rails.application.credentials[Rails.env.to_sym][:FORMEO_URL].freeze
    end

    def self.get_inscriptions_tokens_from_session(session_date_formeo_format, session_certification, session_type)
      session_date = FormeoServices::ModelConverter.convert_formeo_date_to_date(session_date_formeo_format)

      if session_type == 'inter'
        session_exams = FormeoServices::Client.get_sessions_inter_between(session_date, session_date, session_certification)
      else
        session_exams = FormeoServices::Client.get_sessions_intra_between(session_date, session_date, session_certification)
      end

      session_exam = session_exams.find { |se| se['sessionDate'] == session_date_formeo_format }
      session_exam.nil? and return nil
      session_exam[session_certification].pluck('inscriptionToken')
    end

    def self.import_inscription_inter(params)
      handle_elements request(http_method: :post,
                              endpoint: ApiServices::FormeoRouter.endpoint_import_inscription_inter,
                              params: parse_params(required_params: params,
                                                   allowed_params: IMPORT_INSCRIPTION_ALLOWED_PARAMS))
    end

    private

      def self.last_updated_param(last_updated_time)
        last_updated_time.strftime('%Y-%m-%dT%H:%M:%S')
      end

      def self.session_exam_params(start_date, end_date, certification = nil, inscription_token_only = nil)
        { formeo_start_date:        FormeoServices::ModelConverter.convert_date_to_formeo_date(start_date),
          formeo_end_date:          FormeoServices::ModelConverter.convert_date_to_formeo_date(end_date),
          formeo_certification:     certification.nil? ? '' : "&certification=#{certification}",
          formeo_json_named_config: inscription_token_only ? '&jsonNamedConfig=session_exam_inscription_token_only' : '' }
      end

      def self.client
        token    = ApiServices::FormeoRouter.get_token
        endpoint = ApiServices::FormeoRouter.get_endpoint

        client = Faraday.new(endpoint) do |c|
          c.request :json
          c.request :authorization, 'Bearer', token
          c.response :json, content_type: 'application/json'
          c.adapter Faraday.default_adapter
        end

        client.ssl.verify = false if Rails.env.development?

        client
      end

      def self.request(http_method:, endpoint:, params: {})
        client.public_send(http_method, endpoint, params)
      end

      def self.handle_nested_elements(request)
        request.body.is_a?(Hash) ? request.body.dig('data', 'items') : request.body
      end

      def self.handle_elements(request)
        request.body
      end

      def self.api_configuration_params(**configuration_params)
        { max: configuration_params[:max] || 10,
          offset: configuration_params[:offset] || 0,
          order: configuration_params[:order] || 'asc',
          sort: configuration_params[:sort] || 'id' }
      end

      def self.parse_params(required_params:, allowed_params:, **configuration_params)
        allowed_params.merge(required_params)
                      .merge(api_configuration_params(**configuration_params))
      end

      COMPANY_ALLOWED_PARAMS = {
        "id": nil,
        "raisonSociale": nil,
        "raisonSocialeAbregee": nil,
        "complementRs": nil,
        "sigle": nil,
        "regroupementAccord": nil,
        "regroupementCapitalistique": nil,
        "motClassement": nil,
        "numSiren": nil,
        "numSiret": nil,
        "telephone": nil,
        "email": nil,
        "actif": nil,
        "etablissementOrigineProspection": nil,
        "etablissementCodeActivite": nil,
        "etablissementAdressePostaleCriteria": {
            "codePostal": nil,
            "pays": nil,
            "voieNom": nil,
            "ville": nil
        },
        "interlocuteurCriteria": {
            "nom": nil,
            "prenom": nil
        }
      }

      USER_ALLOWED_PARAMS = {
        "etablissementCriteria": {
          "id": nil
        }
      }

      IMPORT_INSCRIPTION_ALLOWED_PARAMS = {
        "etablissement": {
          "id": nil,
          "nom": nil,
          "email": nil,
          "adresse": nil,
          "complement": nil,
          "ville": nil,
          "codePostal": nil,
          "pays": nil,
          "telephone": nil,
          "siret": nil,
          "codeNaf": nil,
          "numeroTva": nil
        },
        "interlocuteur": {
          "id": nil,
          "civiliteId": nil,
          "nom": nil,
          "prenom": nil,
          "email": nil,
          "fonctionExacte": nil,
          "adresse": nil,
          "complement": nil,
          "ville": nil,
          "codePostal": nil,
          "pays": nil,
          "telephone": nil,
          "fonction": nil,
          "statutEmployeur": nil
        },
        "codeMarketing": nil,
        "correlationId": nil,
        "ours": {
          "dateDebut": nil,
          "regroupement": nil,
          "pack": nil
        }
      }
  end
end
